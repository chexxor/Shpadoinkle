-- | This module re-exports the complete HTML DSL,

module Shpadoinkle.Html
  ( module Shpadoinkle.Html.Element
  , module Shpadoinkle.Html.Property
  , module Shpadoinkle.Html.Event
  ) where


import           Shpadoinkle.Html.Element
import           Shpadoinkle.Html.Event
import           Shpadoinkle.Html.Property
