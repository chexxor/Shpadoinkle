cd core
hpack
cd ..

cd backends/snabbdom
hpack
cd ../..

cd backends/pardiff
hpack
cd ../..

cd html
hpack
cd ..

cd router
hpack
cd ..

cd lens
hpack
cd ..

cd widgets
hpack
cd ..

cd examples
hpack
cd ..

cd tests
hpack
cd ..

